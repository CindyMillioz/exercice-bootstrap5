# Bootstrap5

## Tech
- HTML
- CSS
- Bootstrap5

## Architecture
- index.html
- readme.md
    - images 
            - app-store-badge.svg
            - bg-cta.jpeg
            - bg-pattern.png
            - demo-screen-1.jpg
            - google-play-badges.svg
            - iphone_6_plus_white_port.png
    -CSS
        - responsive.css
        - style.css

## Consignes 
- Réaliser une page web selon une maquette existante
- Faire une bonne architecture de projet
- Utiliser HTML/CSS
- Utiliser Bootstrap5